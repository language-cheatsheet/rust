# Unit Testing in Rust

Unit tests in rust are done at the bottom of the file that contains the code you intend to test. This may seem weird if you're coming from something like go or python, but it's due to the macro and compile system in Rust. 

To create a unit test, you use a macro, shown below:
```rust
#[cfg(test)]
```
This tells the rust compiler that the code is a unit test, and shouldn't be compiled into the regular binary.

## Assertions
Assertions are done with the `assert_` macros, an example of equals is shown below
```rust
assert_eq!(1, 1)
```
This will assert that 1 is equal 1, yay we pass.

## Example
An example unit test is shown below:
```rust
fn main() {
    println!("Hello, world!");
}
// say_hello() returns a formatted string, 'Hello bob!'
fn say_hello(name: &String) -> String {
    let mut greeting = String::from("Hello ");
    greeting.push_str(name);
    greeting.push_str("!");
    greeting
}

// unit tests are here
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
	// a test for the function say_hello() defined above
    fn test_say_hello() {
        let name = String::from("Bob");
        let answer = String::from("Hello Bob!");
		// we want to pass in our name, and get "Hello Bob!" back out
        assert_eq!(say_hello(&name), answer);
    }
}

```

