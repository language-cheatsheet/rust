# Getting User Input

To get user input in Rust, use the `std::io` package. An example is shown below:
```rust
use std::io;

fn main() {
    println!("Enter your word or whatever ");
    // Create a new variable named 'word'. This variable is mutable, meaning it can
    // be modified
    // We create a new String type for it with the String::new() function call
    let mut word = String::new();

    // Here we call the io.stdin() function. Note that this is really 1 line, but for 
    // 'readability' rust puts things on multiple lines... It's weird
    io.stdin()
        .read_line(&mut word) // pass the input into our mutable 'word' variable
        .expect("Failed to read your word"); // if we get an error in reading, write this message

    println!("Your word is {}", word);
}
```
