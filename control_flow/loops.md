# Loops in Rust

Loops let you run the same code repeatedly, woah!

## Infinite Loop
```rust
loop {
    println!("hello, world!");
}
```
The `loop` keyword lets you loop forever. You can use `break` to break out of the loop.

## While Loops

```
while expresion {
    ... code ...
}
```

## For Loops

```rust
for i in 1..11 {
    println!("Now serving number {}", i); // 1 - 10 will be printed
}
```

## Match Expressions

Match is sort of like `if/else` and `switch` satements. However, it has pattern matching and exahustiveness which make it better.
Match statements must be **exhaustive**. You cannot miss a case within your statement. 

```rust
let x = 3;
match x {
    1 => println!("one is the loneliest number"),
    2 => println!("two's a company"),
    3 => println!("three's a crowd"),
    _ => println!("your number is lame"),
}
```

but it can be more powerful

```rust
int die1 = 1;
int die2 = 5;

match (die1, die2) {
    (1, 1) => println!("snake eyes!"),
    (5, _) | (_, 5) { // any roll where 1 of the two is 5, doesn't matter which one
        println!("you rolled at least 1 5"); // by using {} we can have multiple lines for a match
        println!("move and then roll again");
    },
    _ => println!("move your piece"),
}
```



