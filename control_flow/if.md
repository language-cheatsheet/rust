# If Statements in Rust

```
if expression {
    ... code ...
} else if expression {
    ... code ...
} else {
    ... code ...
}
```
Values can be retrieved from if statement blocks by leaving the value without a semicolon, like so:
```rust
let amount = if day_of_month % 2 == 0 {
    50
} else {
    10
};
```
