# Structs in Rust
`Structs` are custom types that group related data together, such as all the attributes of a hockey player. 

## When are Structs Useful?
When attributes across something are all the same, but the values are different.

Enums are for when you are choosing between a set of values "Choose between apple, banana, or orange"
Structs are for when you have common set of attributes  "multiple recipes that have a list of ingreidents, and a list of instructions"

```rust
enum HockeyPosition {
    Wing,
    Defense,
    Forward,
    Center,
}

struct HockeyPlayer {
    name: String,
    number: u8,
    position: HockeyPosition,
    goals_ytd: u8,
}

fn main () {
    // you have to define all values for your struct
    let mut player = HockeyPlayer {
        name: String::from("Bryan Rust"),
        number: 17,
        position: HockeyPosition::Wing,
        goals_ytd: 7,
    };

    player.goals_ytd += 1; // you can update a __mutable__ value of a struct

    println!("{} has scored {} goals this season",
        player.name,
        player.goals_ytd,);
}
```

## Tuple Structs
Tuple structs have a type for their fields but not a name. These are useful for creating types of tuples that are different from a regular tuple. 


```rust
struct Triangle(u32, u32, u32);

fn is_equilateral(triangle: Triangle) -> bool {
    triangle.0 = triangle.1 && tirangle.1 == triangle.2
}

fn main() {
    let triangle = Triangle(3, 4, 5);
    is_equilateral(triangle);
}
```

### Common Use Case 'New Type Pattern'
A common use for this is a 'new type pattern'.

```rust
struct Meters(u8)

fn add_distance(d1: Meters, d2: Meters) -> Meters {
    Meters(d1.0 + d2.0)
}

fn main () {
    let distance1 = Meters(3);
    let distnace2 u8 = 7;
    let distance3 = add_distance(distance1, distance2); // ! This doesn't work, distance2 is not of type Meters.
    // This is helpful for preventing issues from using the wrong unit types I guess?
}
```

# Methods on Structs
```rust
enum HockeyPosition {
    Wing,
    Defense,
    Forward,
    Center,
}

struct HockeyPlayer {
    name: String,
    number: u8,
    position: HockeyPosition,
    goals_ytd: u8,
}

fn shoot_puck(hockey_player: HockeyPlayer, seconds_remaining: u16) {
    if seconds_remaining < 300 {
        match hockey_player.position {
            HockeyPosition::Center => println!("Goal"),
            _ => println!("sucks to be you"),
        }
    } else {
        println!("Goal!");
    }
}

fn main () {
    // you have to define all values for your struct
    let mut player = HockeyPlayer {
        name: String::from("Bryan Rust"),
        number: 17,
        position: HockeyPosition::Wing,
        goals_ytd: 7,
    };

    player.goals_ytd += 1; // you can update a __mutable__ value of a struct

    println!("{} has scored {} goals this season",
        player.name,
        player.goals_ytd,);
}
```
In the above example, we should have the `shoot_puck` more tightly tied to the `HockeyPlayer`, since only a `HockeyPlayer` should be able to shoot a goal. 
We can fix this with an `Impl`

## Impl
```rust
// We create an impl on the HockeyPlayer struct. Any method defined in the impl can be used 
// on a HockeyPlayer object
impl HockeyPlayer {
    fn shoot_puck(self, seconds_remaining: u16) {
        if seconds_remaining < 300 {
            match self.position {
                HockeyPosition::Center => println!("Goal"),
                _ => println!("sucks to be you"),
            }
        } else {
            println!("Goal!");
        }
    }
}

enum HockeyPosition {
    Wing,
    Defense,
    Forward,
    Center,
}

struct HockeyPlayer {
    name: String,
    number: u8,
    position: HockeyPosition,
    goals_ytd: u8,
}

fn main() {
    // you have to define all values for your struct
    let mut player = HockeyPlayer {
        name: String::from("Bryan Rust"),
        number: 17,
        position: HockeyPosition::Wing,
        goals_ytd: 7,
    };

    player.goals_ytd += 1; // you can update a __mutable__ value of a struct

    println!(
        "{} has scored {} goals this season",
        player.name, player.goals_ytd,
    );

    // call the shoot_puck function for our player of type HockeyPlayer struct
    player.shoot_puck(1000);
}
```

### Associated Functions
* still defined within an `impl` block
* don't take a `self` parameter
* often used to create an instance of the struct

```rust
impl HockeyPlayer {
    fn new(name: String, number: u8, positon: HockeyPosition) -> HockeyPlayer {
        HockeyPlayer {
            name: name,
            number: number,
            position: position,
            goals_ytd: 0, // default value of 0
        }
    }
}

fn main() {
    let mut player = HockeyPlayer::new(
        String::from("Bryan Rust"),
        17,
        HockeyPosition::Wing,
    );
}
```
