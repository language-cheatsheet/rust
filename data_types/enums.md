# Enums in Rust
`Enums` are a way to express that a value can be one out of a finite set of possible values, and are one of the ways to define a custom types in rust. 

## Properties of an Enum
* Can only be 1 value at a time
* Can enumerate all possible values


## Code

```rust
enum HockeyPosition {
    Center,
    Wing,
    Defense,
    Goalie,
}

fn next_player(position: HockeryPosition) {
    // do something
}

fn main() {
    // make a 'Defense' position
    let position = HockeyPosition::Defense;
    next_player(position);
}
```

## Using Enums with Match
Enums can hold additional data
```rust
enum Clock {
    Sundial(u8),
    Digital(u8, u8),
    Analog(u8, u8, u8),
}

fn tell_time(clock: Clock) {
    match clock {
        Clock::Sundial(hours) =>
            println!("it's about {} o'clock", hours),
        Clock::Analog(hours, minutes, seconds) => {
            println!("It's {} minutes and {} seconds past {} o'lock",
                minutes, seconds, hours);
        },
        Clock::Digital(hours, minutes) =>
            println!("It's {} minutes past {}", minutes, hours),
    }
}

fn main() {
    tell_time(Clock::Analog(9, 25, 45)); // "It's 25 minutes and 45 seconds past 9 o'clock"
}
```

## Named Fields in Enums
This looks like structs.

```rust
enum Clock {
    Sundial { hours: u8 },
    Digital { hours: u8, minutes: u8 },
    Analog { hours: u8, minutes: u8, seconds: u8 },
}

fn main() {
    let clock = Clock::Analog {
        hours: 9,
        minutes: 25,
        seconds: 46,
    };
}
```
