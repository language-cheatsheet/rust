# Ownership in Rust
Ownership is the tricky part of rust. Let's go through some examples of what happens in different scenarios.

## Ex 1
Would this compile?
```rust
fn main() {
    let s = String::from("Hello");
    let s1 = &s;
    let s2 = &s;
    println!("{} {} {}", s, s1, s2);
}
```

<p>
<details>
<summary>Click this to reveal the answer.</summary>
__Yes__. This is because nothing is mutable, so nothing is changing. You can have as many references as you want to an immutable object.
</details>
</p>


## Ex 2
Would this compile?
```rust
fn main() {
    let s = String::from("Hello");
    let s1 = &mut s;
    let s2 = &s;
    println!("{} {} {}", s, s1, s2);
}
```
<p>
<details>
<summary>Click this to reveal the answer.</summary>
__No__. Because you can only have *one* mutable reference to something, and do nothing else. Or, you can have as many people reading as you want.
</details>
</p>


## Ex 3
Would this compile?
```rust
fn main() {
    let s = String::from("Hello");
    let s1 = &mut s;
    println!("{} {} {}", s, s1);
}
```
<p>
<details>
<summary>Click this to reveal the answer.</summary>
__No__. Because `s` is immutable. You cannot take a mutable reference from an immutable object.
</details>
</p>


## Ex 4
```rust
fn main() {
    let mut s = String::from("Hello");
    let s1 = &mut s;
    println!("{} {} {}", s, s1);
}
```
<p>
<details>
<summary>Click this to reveal the answer.</summary>
__No__. You have two mutable references trying to be used at the same time.
</details>
</p>


## Ex 5
```rust
fn main() {
    let mut s = String::from("Hello");
    let s1 = &mut s;
    println!("{}", s1);
    println!("{}", s);
}
```
<p>
<details>
<summary>Click this to reveal the answer.</summary>
__Yes__. This is because the rust compiler is smart. In the lifetime of `s1`, nothing changes to it's reference destination.

However, this does not work:
```rust
    println!("{}", s);
    println!("{}", s1);
```
because you use `s` before `s1` dies.
</details>
</p>



