# Ownership in Rust

Ownership is Rust's stragey for managing data in memory and preventing common problems. Every chunk of memory must have one variable that is the memory's owner. 
The owner is responsible for cleaning up, or *deallocating* that chunk of memory. This cleanup happens automatically when the owner variable goes out of scope. The owner of data is also who is respnosible for decidng whether that data is mutable or not.

Imagine a piece of data as a dish of food that Jane owns. She brings it to a party to share with everyone. As the owner of the dish, she is responsible for cleaning up any leftovers and taking the dish with her when she leaves at the end of the evening.
One problem that ownership solves is that leaving the dish around the host's house would be rude and mean that the host's house gets more and more messy the more parties they have. Eventually, the house would run out of space to have any parties in at all.

## Strings

The `string` type has data that needs to be deallocated when it goes out of scope.

```rust
fn main() {
    let mut a = String::from("Hello");
    a.push_str(" there"); // add " there" to the variable a
    println!("I say {}", a);
    let s = a;
    a.push_str(" bob"); // ! error. a is no longer valid, as s owns that data
}
```

```rust
fn say(s: String) {
    println!("I say, {}", s);
}

fn main() {
    let mut a = String::from("Hello");
    say(a);
    println!("Can i call a again? {}", a); // Error! a moved into 'say()'
}
```

** a cheap hack is to use** `.clone()`

### Exercise
```rust

```
