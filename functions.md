# Functions in Rust

Functions are defined with the keyword `fn` and curly braces. An example is shown below:
```rust
fn hello_world() {
    println!("Hello, world!");
}
```

## Parameters
To put parameters in your function definition you need to put the name and the parameter's data type:
```rust
fn hello_world(age: i32) {
    println!("Hello world! You are {} years old", age);
}
```

## Anonymous Functions
You can do nested anonymous functions like so:

```rust
fn main() {
    let x = 5;

    let y = {
        let x = 3;
        x + 1
    };

    println!("The value of y is: {}", y);
}
```

## Function Returns
To return from a function, put the type in the function parameter, like shown below:
```rust
fn five() -> i32 {
    5
}
```
To return from the function, you simply have the statement not have a semi-colon. Which is confusing but hey so is all of rust.

Another example:
```rust
fn main() {
    let x = plus_one(5);

    println!("The value of x is: {}", x); // == 6
}

fn plus_one(x: i32) -> i32 {
    x + 1
}
```
